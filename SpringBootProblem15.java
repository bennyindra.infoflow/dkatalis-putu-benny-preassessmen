public class SpringBootProblem15 {
public static void main(String[] args) {
		long diff=  DatatypeConverter.parseDateTime("2019-08-25T02:10:24+07:00").getTimeInMillis()-DatatypeConverter.parseDateTime("2018-09-25T02:10:24+07:00").getTimeInMillis();
		long diffSeconds = diff / 1000 % 60;
		long diffMinutes = diff / (60 * 1000) % 60;
		long diffHours = diff / (60 * 60 * 1000) % 24;
		long diffDays = diff / (24 * 60 * 60 * 1000);

		System.out.print(diffDays + " days, ");
		System.out.print(diffHours + " hours, ");
		System.out.print(diffMinutes + " minutes, ");
		System.out.print(diffSeconds + " seconds.");
	}
}
